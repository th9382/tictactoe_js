(function() {
  "use strict";

  if (typeof TTT === 'undefined') {
    window.TTT = {};
  }

  var inherits = TTT.inherits = function(child, parent) {
    function Surrogate() {}
    Surrogate.prototype = parent.prototype;
    child.prototype = new Surrogate();
    child.constructor = child;
  };

  var Player = TTT.Player = function(playerName, symbol) {
      this.playerName = playerName;
      this.symbol = symbol;
      this.playerType = 'human';
  };

  Player.prototype.playTurn = function(game, cb) {
    $(".wrapper").one('click', 'div.cell', function(event) {
      event.preventDefault();
      var $target = $(event.currentTarget);
      var tileId = $target.attr("id");
      var pos = game.getPos(tileId);
      if (!game.active) {
        return;
      }

      if (game.board.valid(pos)) {
        game.board.updateTile(pos, this);
        cb();
      } else {
        this.playTurn(game, cb);
      }
    }.bind(this));
  };

  Player.prototype.handleClick = function(event) {

    var updatedTile = this.board.updateTile(pos, this.currentPlayer);
    this.board.render(this.$el);
  };

  var ComputerPlayer = TTT.ComputerPlayer = function(playerName, symbol) {
    this.playerName = playerName;
    this.symbol = symbol;
    this.playerType = 'ai-easy';
  };

  ComputerPlayer.prototype.playTurn = function(game, cb) {
    var pos = this.winnerMove(game) || this.bestMove(game) ||
      this.randomMove(game);

    if (!game.active) {
      return;
    }
    if (game.board.valid(pos)) {
      setTimeout(function() {
        game.board.updateTile(pos, this);
        cb();
      }.bind(this), 500);
    }
  };

  ComputerPlayer.prototype.bestMove = function(game) {
    return null;
  };

  ComputerPlayer.prototype.winnerMove = function(game) {
    var mark = this.symbol;
    for(var row = 0; row < 3; row++) {
      for(var col = 0; col < 3; col++) {
        var pos = [row, col];
        var board = game.board.dup();

        if (!board.empty(pos)) {
          continue;
        }
        board.placeMark(pos, mark);
        if (board.winner() === mark) {
          return pos;
        }
      }
    }
    // no winning move
    return null;
  };

  ComputerPlayer.prototype.randomMove = function(game) {
    var board = game.board;
    while (true) {
      var randX = Math.floor(Math.random() * 3);
      var randY = Math.floor(Math.random() * 3);
      var pos = [randX, randY];
      if (board.empty(pos)) {
        return pos;
      }
    }
  };

  var SuperComputerPlayer = TTT.SuperComputerPlayer = function(name, symbol) {
    ComputerPlayer.call(this, name, symbol);
    this.playerType = 'ai-hard';
  };

  inherits(SuperComputerPlayer, ComputerPlayer);

  SuperComputerPlayer.prototype.bestMove = function(game) {
    var mark = this.symbol;
    var node = new TTT.TicTacToeNode(game.board, mark);
    var nodeChildren = _.shuffle(node.children());
    var moveNode = _.find(nodeChildren, function(childNode) {
      return childNode.winningNode(mark);
    });

    if (moveNode) {
      return moveNode.prevMovePos;
    }

    moveNode = _.find(nodeChildren, function(childNode) {
      return !childNode.losingNode(mark);
    });

    if (moveNode) {
      return moveNode.prevMovePos;
    }

    return null;
  };

  TTT.createPlayer = function(options) {
    switch(options.type) {
      case "human":
        return new TTT.Player(options.name, options.symbol);
      case "ai-easy":
        return new TTT.ComputerPlayer(options.name, options.symbol);
      case "ai-hard":
        return new TTT.SuperComputerPlayer(options.name, options.symbol);
    }
  };
})();
