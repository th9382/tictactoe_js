(function() {
  "use strict";

  if (typeof TTT === 'undefined') {
    window.TTT = {};
  }

  var SettingsView = TTT.SettingsView = function() {
    this.template = $("#intro-template").html();
    $("#content").html(this.template);
    $(".start-game").one('click', this.handleClickEvent.bind(this));
  };

  SettingsView.prototype.handleClickEvent = function(event) {
    event.preventDefault();
    var gameSource = $("#game-template").html();
    var p1Type = $("input[name='player1-type']:checked").val();
    var p1Name = $("#p1-name").val() || "Player 1";
    var p2Type = $("input[name='player2-type']:checked").val();
    var p2Name = $("#p2-name").val() || "Player 2";

    var player1 = new TTT.createPlayer({
      name: p1Name,
      type: p1Type,
      symbol: "O"
    });
    var player2 = new TTT.createPlayer({
      name: p2Name,
      type: p2Type,
      symbol: "X"
    });

    $("#content").html(gameSource);

    TTT.newGame(player1, player2);
  };
})();
