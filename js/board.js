(function() {
  "use strict";

  if (typeof TTT === 'undefined') {
    window.TTT = {};
  }

  var Tile = TTT.Tile = function(row, column) {
    this.content = " ";
    this.row = row;
    this.column = column;
    this.htmlClass = "cell";
  };

  Tile.prototype.setContentValue = function(content) {
    this.content = content;
  };

  Tile.prototype.equals = function(other) {
    if (this.content === " ") {
      return false;
    }
    return this.content === other.content;
  };

  var Board = TTT.Board = function(board) {
    this.board = board || this.buildBoard();
  };

  Board.prototype.buildBoard = function() {
    var board = [];
    for(var row = 0; row < 3; row++) {
      var rows = [];
      for(var col = 0; col < 3; col++) {
        var tile = new Tile(row, col);
        rows.push(tile);
      }
      board.push(rows);
    }
    return board;
  };

  Board.prototype.render = function($el) {
    $el.children().remove();
    for(var i = 0; i < this.board.length; i++) {
      for(var j = 0; j < this.board[i].length; j++) {
        var tile = this.board[i][j];
        var $tile = $('<div>');
        $tile.attr('id','c_' + tile.row + '_' + tile.column);
        $tile.addClass(tile.htmlClass);
        $tile.html('<span>' + tile.content + '</span>');
        $el.append($tile);
      }
    }
  };

  Board.prototype.dup = function() {
    var dupTiles = _.cloneDeep(this.board);
    var newBoard = new Board(dupTiles);
    return newBoard;
  };

  Board.prototype.empty = function(pos) {
    if (this.valid(pos)) {
      return this.getTile(pos).content === " ";
    }
  };

  Board.prototype.placeMark = function(pos, mark) {
    if (this.valid(pos)) {
      this.getTile(pos).content = mark;
    }
  };

  Board.prototype.valid = function(pos) {
    return pos[0] < 3 && pos[0] >= 0 &&
      pos[1] < 3 && pos[1] >= 0 &&
        this.getTile(pos).content === " ";
  };

  Board.prototype.getTile = function(pos) {
    return this.board[pos[0]][pos[1]];
  };

  Board.prototype.columns = function() {
    var cols = [];
    for(var i = 0; i < 3; i++) {
      cols.push([]);
    }

    for(var row = 0; row < 3; row++) {
      for(var col = 0; col < 3; col++) {
        cols[col][row] = this.rows()[row][col];
      }
    }
    return cols;
  };

  Board.prototype.rows = function() {
    return this.board;
  };

  Board.prototype.diagonals = function() {
    return [
            [this.board[0][0],this.board[1][1], this.board[2][2]],
            [this.board[0][2], this.board[1][1], this.board[2][0]]
          ];
  };

  Board.prototype.won = function () {
    if (this.winner()) {
      return true;
    }

    return false;
  };

  Board.prototype.over = function() {
    return this.won() || this.isDraw();
  };

  Board.prototype.winner = function() {
    var arr = this.rows().concat(this.columns(), this.diagonals());
    for(var i = 0, n = arr.length; i < n; i++) {
      var section = arr[i];
      var marksStr = _.map(section, function(el) {
        return el.content;
      }).join('');

      if (marksStr === "OOO") {
        return "O";
      } else if (marksStr === "XXX") {
        return "X";
      }
    }
    return null;
  };

  Board.prototype.isDraw = function() {
    if (this.won()) {
      return false;
    }
    var mergedArr = [].concat.apply([], this.board);
    return !mergedArr.some(function(tile) {
      return tile.content === " ";
    });
  };

  //TODO refactor this.
  Board.prototype.updateTile = function(pos, player) {
    var current_tile = this.getTile(pos);
    if( current_tile.content === " " ) {
      current_tile.setContentValue(player.symbol);
      if( player.symbol === "O" ) {
        current_tile.htmlClass += " p1";
      } else {
        current_tile.htmlClass += " p2";
      }
      return true;
    } else {
      return false;
    }
  };
})();
