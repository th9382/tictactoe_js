(function() {
  "use strict";

  if (typeof TTT === 'undefined') {
    window.TTT = {};
  }

  var Game = TTT.Game = function(options) {
    this.$el = $(options.el);
    this.$status = $(options.statusEl);
    this.active = true;
    this.player1 = options.player1;
    this.player2 = options.player2;
    this.currentPlayer = this.player1;
    this.board = new TTT.Board();
    this.board.render(this.$el);
    this.$status.text(this.currentPlayer.playerName + "'s turn");
    this.$el.removeClass('animated shake');
    this.bindEvents();
    this.run();
  };

  Game.prototype.run = function() {
    if (this.board.over()) {
        this.gameOver();
    } else {
      this.currentPlayer.playTurn(this, function() {
        this.switchPlayer();
        this.board.render(this.$el);
        this.run();
      }.bind(this));
    }
  };

  Game.prototype.bindEvents = function(event) {
    $(document).one('keydown', this.handleKeyPress.bind(this));
    $(document).one('click', 'button.restart', this.restart.bind(this));
    $(document).one('click', 'button.settings', this.changeSettings.bind(this));
  };

  Game.prototype.handleKeyPress = function(event) {
    event.preventDefault();
    if (event.keyCode === 82) { // 'r' for resetting game
      this.restart(event);
    } else if ( event.keyCode === 83){ // 's' to change game setting
      this.changeSettings();
    }
  };

  Game.prototype.changeSettings = function(event) {
    new TTT.SettingsView();
  };

  Game.prototype.restart = function(event) {
    this.active = false;
    var player1 = new TTT.createPlayer({
      type: this.player1.playerType,
      name: this.player1.playerName,
      symbol: this.player1.symbol
    });
    var player2 = new TTT.createPlayer({
      type: this.player2.playerType,
      name: this.player2.playerName,
      symbol: this.player2.symbol
    });

    TTT.newGame(player1, player2);
  };

  Game.prototype.gameOver = function() {
    if (this.board.won()) {
      var winner = this.getPlayer(this.board.winner());
      this.$status.text(winner.playerName + ' wins!');
    } else if (this.board.isDraw()) {
      this.$status.text("It's a draw!");
    }
    this.$el.addClass('animated shake');
    this.active = false;
  };

  Game.prototype.getPlayer = function(mark) {
    return this.player1.symbol === mark ? this.player1 : this.player2;
  };

  Game.prototype.switchPlayer = function() {
    this.currentPlayer = this.currentPlayer == this.player1 ? this.player2 : this.player1;
    this.$status.text(this.currentPlayer.playerName + "'s turn");
  };

  Game.prototype.getPos = function(string) {
    var arr = string.split('_');
    var row = parseInt(arr[1]);
    var column = parseInt(arr[2]);
    return [row, column];
  };

  TTT.newGame = function(player1, player2) {
    var game = new TTT.Game({
      el: '.wrapper',
      statusEl: '#status',
      player1: player1,
      player2: player2
    });
  };
})();
