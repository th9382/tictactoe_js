# Tic-Tac-Toe

Tic-Tac-Toe is a two player game, where players take turns marking the spaces in a 3×3 grid. The player who succeeds in placing three respective marks in a horizontal, vertical, or diagonal row wins the game.

You can play v.s. another person or against a simple or smart computer. Or, for fun, you can also have computers play each other.

Click on a square to place a mark. Alternate turns. Press 'r' to restart or 's' to change settings. Enjoy!


### [Live Demo][weburl]

[weburl]: https://th11.github.io/ticTacToe_js/html/ttt.html

## Features
- [x] 2-player game or 1-player v.s. computer
- [x] Settings view to specify opponent
- [x] Simple AI
- [x] Smart AI
- [x] Seamless restart

## Technologies
- jQuery
- lodash
- Bootstrap
- Animate.css

## TODOs
- Update smart AI to take winning move if possible
- Add css spinner when AI's turn... to make it seem like it's thinking hard.
- Update Easy AI - try to block if opponent has a winning move coming up

## Screenshot
![ss]
[ss]: ./images/ttt.png
